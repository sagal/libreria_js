import xml2js from 'https://esm.sh/xml2js?pin=v55'
import axiod from "https://deno.land/x/axiod/mod.ts";

const clientId = Deno.env.get('CLIENT_ID')
const clientIntegrationId = Deno.env.get('CLIENT_INTEGRATION_ID')
const clientEcommerce = JSON.parse(Deno.env.get('CLIENT_ECOMMERCE'))

const username = Deno.env.get('USER')
const password = Deno.env.get('PASS')

const libreriaUrl = "http://americalatina1.dyndns.org/WsGenQueryFacade/wsGenQuery.asmx"

const articleRequest = `
                            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsg="http://wsGenQueryFacade/">
                                <soapenv:Header/>
                                <soapenv:Body>
                                    <wsg:GetData>
                                        <!--Optional:-->
                                        <wsg:keyCode>LIBROS</wsg:keyCode>
                                        <!--Optional:-->
                                        <wsg:parameters>
                                            <![CDATA[<params><fecha>20170101</fecha></params>]]>
                                        </wsg:parameters>
                                    </wsg:GetData>
                                </soapenv:Body>
                            </soapenv:Envelope>
                        `


const init = async () => {
    let articles = await fetchArticles();
    let ecommerceValues = Object.values(clientEcommerce)
    let payloads = []
    for (let article of articles) {
        try {
            let payload = {
                sku: article.codigo[0].trim(),
                client_id: clientId,
                integration_id: clientIntegrationId,
                options: {
                    merge: false,
                    partial: true
                },
                ecommerce: ecommerceValues.map(ecommerce_id => {
                    return {
                        ecommerce_id: ecommerce_id,
                        properties: [
                            { "name": article.descripcion[0] },
                            {
                                "price": {
                                    "value": parseFloat(article.precio[0]),
                                    "currency": "$"
                                }
                            }
                        ],
                        variants: []
                    }
                })
            }
            payloads.push(payload)
        } catch (e) {
            console.log(`Could not dispatch article: ${e}`)
        }
    }
    await sagalDispatchBatch({
        products: payloads,
        batch_size: 100
    })
}

async function fetchArticles() {
    try {
        let response = await axiod.post(libreriaUrl, articleRequest, {
            headers: {
                "Content-Type": "text/xml;charset=UTF-8",
                "Authorization": 'Basic ' + btoa(username + ":" + password)
            },
            timeout: 500000
        });
        let parser = new xml2js.Parser();
        response = response.data.replaceAll("&lt;", "<")
        response = response.replaceAll("&gt;", ">")
        response = await parser.parseStringPromise(response)
        return response["soap:Envelope"]["soap:Body"][0]["GetDataResponse"][0]["GetDataResult"][0]["NewDataSet"][0]["Table"]

    } catch (e) {
        console.log(e)
    }
}


await init();
